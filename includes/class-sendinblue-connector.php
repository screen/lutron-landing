<?php

/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/**
 * Sendinblue Custom Functions
 */

function sendinblue_create_contact($email, $list_id, $data)
{
    $sendinblue_settings = get_option('lutron_sendinblue_settings');
    $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', $sendinblue_settings['apikey']);
    $apiInstance = new SendinBlue\Client\Api\ContactsApi(
        // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
        // This is optional, `GuzzleHttp\Client` will be used as default.
        new GuzzleHttp\Client(),
        $config
    );
    $createContact = new \SendinBlue\Client\Model\CreateContact(); // \SendinBlue\Client\Model\CreateContact | Values to create a contact
    $createContact['email'] = $email;
    $arr_list_id = explode(',', $list_id);
    foreach ($arr_list_id as $item) {
        $arr_list_number[] = intval($item);
    }
    $createContact['listIds'] = $arr_list_number;
    $createContact['attributes'] = $data;
    $createContact['updateEnabled'] = true;
    $createContact['emailBlacklisted'] = false;
    $createContact['smsBlacklisted'] = false;
    $createContact['smtpBlacklistSender'] = [];

    $message_error = '';
    
    try {
        $error = false;
        $result = $apiInstance->createContact($createContact);

        $contactEmails = new \SendinBlue\Client\Model\AddContactToList();
        $contactEmails['emails'] = array($email);

        try {
            $error = false;
            foreach ($arr_list_id as $item) {
                $result = $apiInstance->addContactToList(intval($item), $contactEmails);
            }
        } catch (Exception $e) {
            $error = true;
            //echo 'Exception when calling ContactsApi->addContactToList: ', $e->getMessage(), PHP_EOL;
        }

    } catch (Exception $e) {
        $result = json_decode($e->getResponseBody());
        $error = true;

        switch ($result->code) {
            case 'duplicate_parameter':
                $message_error = esc_html__('This contact is already registered on our systems', 'lutron');
                break;

            case 'invalid_parameter':
                $message_error = 'This contact has an ' . strtolower($result->message) . ', please check your data';
                break;
                
            default:
                $message_error = esc_html__("There's an error on our systems, please try again later", 'lutron');
                break;
        }
    }

    $response = array(
        'error' => $error,
        'message_error' => $message_error
    );

    return $response;
}
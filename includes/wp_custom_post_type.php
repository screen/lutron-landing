<?php

if (!defined('ABSPATH')) {
	die('Invalid request.');
}

/*
function lutron_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'lutron' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'lutron' ),
		'menu_name'             => __( 'Clientes', 'lutron' ),
		'name_admin_bar'        => __( 'Clientes', 'lutron' ),
		'archives'              => __( 'Archivo de Clientes', 'lutron' ),
		'attributes'            => __( 'Atributos de Cliente', 'lutron' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'lutron' ),
		'all_items'             => __( 'Todos los Clientes', 'lutron' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'lutron' ),
		'add_new'               => __( 'Agregar Nuevo', 'lutron' ),
		'new_item'              => __( 'Nuevo Cliente', 'lutron' ),
		'edit_item'             => __( 'Editar Cliente', 'lutron' ),
		'update_item'           => __( 'Actualizar Cliente', 'lutron' ),
		'view_item'             => __( 'Ver Cliente', 'lutron' ),
		'view_items'            => __( 'Ver Clientes', 'lutron' ),
		'search_items'          => __( 'Buscar Cliente', 'lutron' ),
		'not_found'             => __( 'No hay resultados', 'lutron' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'lutron' ),
		'featured_image'        => __( 'Imagen del Cliente', 'lutron' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'lutron' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'lutron' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'lutron' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'lutron' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'lutron' ),
		'items_list'            => __( 'Listado de Clientes', 'lutron' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'lutron' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'lutron' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'lutron' ),
		'description'           => __( 'Portafolio de Clientes', 'lutron' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'lutron_custom_post_type', 0 );
*/

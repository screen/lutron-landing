<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action('customize_register', 'lutron_customize_register');

function lutron_customize_register($wp_customize)
{

    /* HEADER SETTINGS */
    $wp_customize->add_section('ltr_header_settings', array(
        'title'    => __('Header', 'lutron'),
        'description' => __('Header elements options', 'lutron'),
        'priority' => 30
    ));

    $wp_customize->add_setting('ltr_header_settings[text]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('text', array(
        'type' => 'text',
        'label'    => __('Texto del top header', 'lutron'),
        'description' => __('Agregue el Texto del top header', 'lutron'),
        'section'  => 'ltr_header_settings',
        'settings' => 'ltr_header_settings[text]'
    ));

    $wp_customize->add_setting('ltr_header_settings[phone]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('phone', array(
        'type' => 'text',
        'label'    => __('Teléfono (Link)', 'lutron'),
        'description' => __('Agregue el teléfono en formato link que será usado en el sitio', 'lutron'),
        'section'  => 'ltr_header_settings',
        'settings' => 'ltr_header_settings[phone]'
    ));

    $wp_customize->add_setting('ltr_header_settings[phone_text]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('phone_text', array(
        'type' => 'text',
        'label'    => __('Teléfono (Texto)', 'lutron'),
        'description' => __('Agregue el teléfono que será usado en el sitio', 'lutron'),
        'section'  => 'ltr_header_settings',
        'settings' => 'ltr_header_settings[phone_text]'
    ));

    /* SOCIAL SETTINGS */
    $wp_customize->add_section('ltr_social_settings', array(
        'title'    => __('Redes Sociales', 'lutron'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'lutron'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('ltr_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'lutron_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('facebook', array(
        'type' => 'url',
        'section' => 'ltr_social_settings',
        'settings' => 'ltr_social_settings[facebook]',
        'label' => __('Facebook', 'lutron'),
    ));

    $wp_customize->add_setting('ltr_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'lutron_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('twitter', array(
        'type' => 'url',
        'section' => 'ltr_social_settings',
        'settings' => 'ltr_social_settings[twitter]',
        'label' => __('Twitter', 'lutron'),
    ));

    $wp_customize->add_setting('ltr_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'lutron_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('instagram', array(
        'type' => 'url',
        'section' => 'ltr_social_settings',
        'settings' => 'ltr_social_settings[instagram]',
        'label' => __('Instagram', 'lutron'),
    ));

    $wp_customize->add_setting('ltr_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'lutron_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('linkedin', array(
        'type' => 'url',
        'section' => 'ltr_social_settings',
        'settings' => 'ltr_social_settings[linkedin]',
        'label' => __('LinkedIn', 'lutron'),
    ));

    $wp_customize->add_setting('ltr_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'lutron_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('youtube', array(
        'type' => 'url',
        'section' => 'ltr_social_settings',
        'settings' => 'ltr_social_settings[youtube]',
        'label' => __('YouTube', 'lutron'),
    ));

    /* COOKIES SETTINGS */
    $wp_customize->add_section('ltr_cookie_settings', array(
        'title'    => __('Cookies', 'lutron'),
        'description' => __('Opciones de Cookies', 'lutron'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('ltr_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'lutron'),
        'description' => __('Texto del Cookie consent.'),
        'section'  => 'ltr_cookie_settings',
        'settings' => 'ltr_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('ltr_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'ltr_cookie_settings',
        'settings' => 'ltr_cookie_settings[cookie_link]',
        'label' => __('Link de Cookies', 'lutron'),
    ));

    /* GOOGLE RECAPTCHA */
    $wp_customize->add_section('lutron_google_settings', array(
        'title'    => __('Google', 'lutron'),
        'description' => __('Google Recaptcha Data', 'lutron'),
        'priority' => 180,
    ));

    $wp_customize->add_setting('lutron_google_settings[sitekey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('sitekey', array(
        'type' => 'text',
        'label'    => __('SiteKey', 'lutron'),
        'description' => __('Enter Recaptcha API SiteKey.'),
        'section'  => 'lutron_google_settings',
        'settings' => 'lutron_google_settings[sitekey]'
    ));

    $wp_customize->add_setting('lutron_google_settings[secretkey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('secretkey', array(
        'type' => 'text',
        'label'    => __('SecretKey', 'lutron'),
        'description' => __('Enter Recaptcha API SecretKey.'),
        'section'  => 'lutron_google_settings',
        'settings' => 'lutron_google_settings[secretkey]'
    ));

    /* SENDINBLUE SETTINGS */
    $wp_customize->add_section('lutron_sendinblue_settings', array(
        'title'    => __('Sendinblue', 'lutron'),
        'description' => __('Opciones para Sendinblue', 'lutron'),
        'priority' => 180
    ));

    $wp_customize->add_setting('lutron_sendinblue_settings[apikey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('apikey', array(
        'type' => 'text',
        'label'    => __('APIKey', 'lutron'),
        'description' => __('Agregar APIkey de Sendinblue', 'lutron'),
        'section'  => 'lutron_sendinblue_settings',
        'settings' => 'lutron_sendinblue_settings[apikey]'
    ));

    $wp_customize->add_setting('lutron_sendinblue_settings[thanks_link]', array(
        'default'           => '',
        'sanitize_callback' => 'lutron_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('thanks_link', array(
        'type'     => 'text',
        'section' => 'lutron_sendinblue_settings',
        'settings' => 'lutron_sendinblue_settings[thanks_link]',
        'label' => __('Página de Agradecimiento', 'lutron'),
    ));

    /* EMAIL SETTINGS */
    $wp_customize->add_section('lutron_email_settings', array(
        'title'    => __('Correos Electronicos', 'lutron'),
        'description' => __('Opciones para Correos Electronicos', 'lutron'),
        'priority' => 178,
    ));

    $wp_customize->add_setting('lutron_email_settings[main_email]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('main_email', array(
        'type' => 'text',
        'label'    => __('Correo Principal', 'lutron'),
        'description' => __('Colocar aqui el correo principal para los formularios.'),
        'section'  => 'lutron_email_settings',
        'settings' => 'lutron_email_settings[main_email]'
    ));

    $wp_customize->add_setting('lutron_email_settings[cc_email]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('cc_email', array(
        'type' => 'text',
        'label'    => __('Correos en copia', 'lutron'),
        'description' => __('Colocar aqui el correo o correos que estarán en copia para los formularios.'),
        'section'  => 'lutron_email_settings',
        'settings' => 'lutron_email_settings[cc_email]'
    ));

    $wp_customize->add_setting('lutron_email_settings[bcc_email]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('bcc_email', array(
        'type' => 'text',
        'label'    => __('Correos en copia oculta', 'lutron'),
        'description' => __('Colocar aqui el correo o correos que estarán en copia oculta para los formularios.'),
        'section'  => 'lutron_email_settings',
        'settings' => 'lutron_email_settings[bcc_email]'
    ));
}

function lutron_sanitize_url($url)
{
    return esc_url_raw($url);
}

/* --------------------------------------------------------------
CUSTOM CONTROL PANEL
-------------------------------------------------------------- */
/*
function register_lutron_settings() {
    register_setting( 'lutron-settings-group', 'monday_start' );
    register_setting( 'lutron-settings-group', 'monday_end' );
    register_setting( 'lutron-settings-group', 'monday_all' );
}

add_action('admin_menu', 'lutron_custom_panel_control');

function lutron_custom_panel_control() {
    add_menu_page(
        __( 'Panel de Control', 'lutron' ),
        __( 'Panel de Control','lutron' ),
        'manage_options',
        'lutron-control-panel',
        'lutron_control_panel_callback',
        'dashicons-admin-generic',
        120
    );
    add_action( 'admin_init', 'register_lutron_settings' );
}

function lutron_control_panel_callback() {
    ob_start();
?>
<div class="lutron-admin-header-container">
    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="lutron" />
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
</div>
<form method="post" action="options.php" class="lutron-admin-content-container">
    <?php settings_fields( 'lutron-settings-group' ); ?>
    <?php do_settings_sections( 'lutron-settings-group' ); ?>
    <div class="lutron-admin-content-item">
        <table class="form-table">
            <tr valign="center">
                <th scope="row"><?php _e('Monday', 'lutron'); ?></th>
                <td>
                    <label for="monday_start">Starting Hour: <input type="time" name="monday_start" value="<?php echo esc_attr( get_option('monday_start') ); ?>"></label>
                    <label for="monday_end">Ending Hour: <input type="time" name="monday_end" value="<?php echo esc_attr( get_option('monday_end') ); ?>"></label>
                    <label for="monday_all">All Day: <input type="checkbox" name="monday_all" value="1" <?php checked( get_option('monday_all'), 1 ); ?>></label>
                </td>
            </tr>
        </table>
    </div>
    <div class="lutron-admin-content-submit">
        <?php submit_button(); ?>
    </div>
</form>
<?php
    $content = ob_get_clean();
    echo $content;
}
*/
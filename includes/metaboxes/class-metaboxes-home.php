<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/**
 * Home Custom Metaboxes
 *
 * @package lutron
 */

 class customHomeMetaboxes extends customCMB2Class
 {
     /**
      * Main Constructor.
      */
     public function __construct()
     {
         add_action('cmb2_admin_init', array($this, 'home_metaboxes'));
         add_action('cmb2_admin_init', array($this, 'general_metaboxes'));
         add_action('cmb2_admin_init', array($this, 'landingA_metaboxes'));
         add_action('cmb2_admin_init', array($this, 'landingB_metaboxes'));
     }

     public function landingA_metaboxes() {
        $cmb_landingA_hero = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'landingA_hero_metabox',
            'title'         => esc_html__('Home: Main Hero Landing A', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        $cmb_landingA_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingA_form_title',
            'name'      => esc_html__('Título del formulario en Landing A', 'lutron'),
            'desc'      => esc_html__('Ingrese el título de este formulario', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_landingA_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingA_form_subtitle',
            'name'      => esc_html__('Subtítulo del formulario en Landing A', 'lutron'),
            'desc'      => esc_html__('Ingrese el subtítulo de este formulario', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_landingA_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingA_form_btn_text',
            'name'      => esc_html__('Texto del boton del formulario en Landing A', 'lutron'),
            'desc'      => esc_html__('Ingrese el Texto del CTA de este formulario', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_landingA_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingA_sendinblue_list_id',
            'name'      => esc_html__('ID de la Lista en Sendinblue formulario en Landing A', 'lutron'),
            'desc'      => esc_html__('Ingrese el ID de la Lista en Sendinblue de este formulario', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_landingA_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingA_thanks_page',
            'name'      => esc_html__('Página de agradecimiento al completar formulario en Landing A', 'lutron'),
            'desc'      => esc_html__('Ingrese el URL de la pagina de agradecimiento a redirigir al momento de completar el fomulario', 'lutron'),
            'type'      => 'text_url'
        ));
     }

     public function landingB_metaboxes() {
        $cmb_landingB_hero = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'landingB_hero_metabox',
            'title'         => esc_html__('Home: Main Hero Landing B', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        $cmb_landingB_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingB_form_image',
            'name'      => esc_html__('Hero Main Image', 'lutron'),
            'desc'      => esc_html__('Upload a background for this hero', 'lutron'),
            'type'      => 'file',

            'options' => array(
                'url' => false
            ),
            'text'    => array(
                'add_upload_file_text' => esc_html__('Cargar fondo', 'lutron'),
            ),
            'query_args' => array(
                'type' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            ),
            'preview_size' => 'thumbnail'
        ));

        $cmb_landingB_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingB_form_title',
            'name'      => esc_html__('Título del formulario en Landing B', 'lutron'),
            'desc'      => esc_html__('Ingrese el título de este formulario', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_landingB_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingB_form_subtitle',
            'name'      => esc_html__('Subtítulo del formulario en Landing B', 'lutron'),
            'desc'      => esc_html__('Ingrese el subtítulo de este formulario', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_landingB_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingB_form_btn_text',
            'name'      => esc_html__('Texto del boton del formulario en Landing B', 'lutron'),
            'desc'      => esc_html__('Ingrese el Texto del CTA de este formulario', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_landingB_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingB_sendinblue_list_id',
            'name'      => esc_html__('ID de la Lista en Sendinblue formulario en Landing B', 'lutron'),
            'desc'      => esc_html__('Ingrese el ID de la Lista en Sendinblue de este formulario', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_landingB_hero->add_field(array(
            'id'        => parent::PREFIX . 'landingB_thanks_page',
            'name'      => esc_html__('Página de agradecimiento al completar formulario en Landing B', 'lutron'),
            'desc'      => esc_html__('Ingrese el URL de la pagina de agradecimiento a redirigir al momento de completar el fomulario', 'lutron'),
            'type'      => 'text_url'
        ));
    }

     public function general_metaboxes()
     {
         if (!empty($_GET['post'])) {
             $post_id = sanitize_key($_GET['post']);
             $template_name = get_post_meta($post_id, '_wp_page_template', true);
         } else {
             $template_name = '';
         }

         $arr_sections = array(
            'main_hero'  => 'Main Hero',
            'main_hero_landingA'  => 'Main Hero Landing A',
            'main_hero_landingB'  => 'Main Hero Landing B',
            'services'   => 'Services',
            'testimonials'   => 'Testimonials',
            'awards'   => 'Logos Awards',
            'black_bar'   => 'Black Hero Bar',
            'portfolio' => 'Portfolio',
            'whoweare' => 'Who we are',
            'team' => 'Equipo',
            'carousel' => 'carousel',
            'contact' => 'Contact'
         );
      
         $cmb_general_metabox = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'general_metabox_metabox',
            'title'         => esc_html__('Landing: Order Sections', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'side',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $cmb_general_metabox->add_field(array(
            'id'        => parent::PREFIX . 'page_order',
            'name'      => esc_html__('Order for Elements', 'lutron'),
            'desc'      => esc_html__('Add / Remove Sections. Drag to Reorder', 'lutron'),
            'type'    => 'pw_multiselect',
            'options' =>  $arr_sections,
            'attributes' => array(
                'placeholder' => 'Select Sections. Drag to reorder'
            ),
        ));
     }

     public function home_metaboxes()
     {
         /* --------------------------------------------------------------
            1.- HOME: SLIDER SECTION
        -------------------------------------------------------------- */
         $cmb_home_hero = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_hero_metabox',
            'title'         => esc_html__('Home: Main Hero', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $cmb_home_hero->add_field(array(
            'id'        => parent::PREFIX . 'home_hero_image',
            'name'      => esc_html__('Hero Main Image', 'lutron'),
            'desc'      => esc_html__('Upload a background for this hero', 'lutron'),
            'type'      => 'file',

            'options' => array(
                'url' => false
            ),
            'text'    => array(
                'add_upload_file_text' => esc_html__('Cargar fondo', 'lutron'),
            ),
            'query_args' => array(
                'type' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            ),
            'preview_size' => 'thumbnail'
        ));

        
         $cmb_home_hero->add_field(array(
            'id'        => parent::PREFIX . 'home_hero_video_mp4',
            'name'      => esc_html__('Hero Main Video Mp4', 'lutron'),
            'desc'      => esc_html__('Upload a video for this hero', 'lutron'),
            'type'      => 'file',

            'options' => array(
                'url' => false
            ),
            'text'    => array(
                'add_upload_file_text' => esc_html__('Upload video', 'lutron'),
            ),
            'query_args' => array(
                'type' => array(
                    'video/mp4'
                )
            ),
            'preview_size' => 'thumbnail'
        ));

         $cmb_home_hero->add_field(array(
            'id'        => parent::PREFIX . 'home_hero_video_webm',
            'name'      => esc_html__('Hero Main Video WebM', 'lutron'),
            'desc'      => esc_html__('Upload a video for this hero', 'lutron'),
            'type'      => 'file',

            'options' => array(
                'url' => false
            ),
            'text'    => array(
                'add_upload_file_text' => esc_html__('Upload video', 'lutron'),
            ),
            'query_args' => array(
                'type' => array(
                    'video/webm'
                )
            ),
            'preview_size' => 'thumbnail'
        ));

         $cmb_home_hero->add_field(array(
            'id'        => parent::PREFIX . 'home_hero_video_ogv',
            'name'      => esc_html__('Hero Main Video Ogv', 'lutron'),
            'desc'      => esc_html__('Upload a video for this hero', 'lutron'),
            'type'      => 'file',

            'options' => array(
                'url' => false
            ),
            'text'    => array(
                'add_upload_file_text' => esc_html__('Upload video', 'lutron'),
            ),
            'query_args' => array(
                'type' => array(
                    'video/ogg',
                )
            ),
            'preview_size' => 'thumbnail'
        ));

         $cmb_home_hero->add_field(array(
            'id'        => parent::PREFIX . 'home_hero_btn_text1',
            'name'      => esc_html__('Texto del boton de la Sección', 'lutron'),
            'desc'      => esc_html__('Ingrese el texto de este botón', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_hero->add_field(array(
            'id'        => parent::PREFIX . 'home_hero_btn_link1',
            'name'      => esc_html__('Link del boton de la Sección', 'lutron'),
            'desc'      => esc_html__('Ingrese el link de este botón', 'lutron'),
            'type'      => 'text_url'
        ));

         $cmb_home_hero->add_field(array(
            'id'        => parent::PREFIX . 'home_hero_btn_text2',
            'name'      => esc_html__('Texto del boton de la Sección', 'lutron'),
            'desc'      => esc_html__('Ingrese el texto de este botón', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_hero->add_field(array(
            'id'        => parent::PREFIX . 'home_hero_btn_link2',
            'name'      => esc_html__('Link del boton de la Sección', 'lutron'),
            'desc'      => esc_html__('Ingrese el link de este botón', 'lutron'),
            'type'      => 'text_url'
        ));

         /* --------------------------------------------------------------
             2.- HOME: GROUP 1
         -------------------------------------------------------------- */
         $cmb_home_group1 = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_group_metabox',
            'title'         => esc_html__('Home: Servicios', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        $cmb_home_group1->add_field(array(
            'id'        => parent::PREFIX . 'home_group_black_bar',
            'name'      => esc_html__('Activar Barra Negra en la mitad de los elementos?', 'lutron'),
            'desc'      => esc_html__('Seleccione si desea mostar la barra negra con el CTa al formulario al momento de llegar a la mitad de los elementos', 'lutron'),
            'type'      => 'checkbox'
        ));

         $group_field_id = $cmb_home_group1->add_field(array(
            'id'            => parent::PREFIX . 'home_group1',
            'name'          => esc_html__('Group 1', 'lutron'),
            'description'   => __('Items inside Section', 'lutron'),
            'type'          => 'group',
            'options'       => array(
                'group_title'       => __('Item {#}', 'lutron'),
                'add_button'        => __('Add Other Item', 'lutron'),
                'remove_button'     => __('Remove Item', 'lutron'),
                'sortable'          => true,
                'closed'            => true,
                'remove_confirm'    => esc_html__('Are you sure to remove this Item?', 'lutron')
            )
        ));
        
         $cmb_home_group1->add_group_field($group_field_id, array(
            'id'        => 'image',
            'name'      => esc_html__('Item Image', 'lutron'),
            'desc'      => esc_html__('Upload an Image for this item', 'lutron'),
            'type'      => 'file',
            'options'   => array(
                'url'   => false
            ),
            'text'      => array(
                'add_upload_file_text' => esc_html__('Upload Image', 'lutron'),
            ),
            'query_args' => array(
                'type'  => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            ),
            'preview_size' => 'thumbnail'
        ));
        
         $cmb_home_group1->add_group_field($group_field_id, array(
            'id'        => 'title',
            'name'      => esc_html__('Item Title', 'lutron'),
            'desc'      => esc_html__('Enter the title for this item', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_group1->add_group_field($group_field_id, array(
            'id'        => 'desc',
            'name'      => esc_html__('Item Description', 'lutron'),
            'desc'      => esc_html__('Enter the description for this item', 'lutron'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

         $cmb_home_group1->add_group_field($group_field_id, array(
            'id'        => 'btn_text',
            'name'      => esc_html__('Button Text', 'lutron'),
            'desc'      => esc_html__('Enter the text for this Button', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_group1->add_group_field($group_field_id, array(
            'id'        => 'btn_link',
            'name'      => esc_html__('Button Link', 'lutron'),
            'desc'      => esc_html__('Enter the url for this Button', 'lutron'),
            'type'      => 'text_url'
        ));

         /* --------------------------------------------------------------
              5.1 - HOME: TESTIMONIALS
          -------------------------------------------------------------- */
         $cmb_home_test = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_testimonials_metabox',
            'title'         => esc_html__('Home: Testimonials', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $group_field_id = $cmb_home_test->add_field(array(
            'id'            => parent::PREFIX . 'home_testimonials',
            'name'          => esc_html__('Testimonials', 'lutron'),
            'description'   => __('Testimonials inside Section', 'lutron'),
            'type'          => 'group',
            'options'       => array(
                'group_title'       => __('Testimonial {#}', 'lutron'),
                'add_button'        => __('Add Other Testimonial', 'lutron'),
                'remove_button'     => __('Remove Testimonial', 'lutron'),
                'sortable'          => true,
                'closed'            => true,
                'remove_confirm'    => esc_html__('Are you sure to remove this Testimonial?', 'lutron')
            )
        ));
        
         $cmb_home_test->add_group_field($group_field_id, array(
            'id'        => 'title',
            'name'      => esc_html__('Testimonial Name', 'lutron'),
            'desc'      => esc_html__('Enter the Name for this Testimonial', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_test->add_group_field($group_field_id, array(
            'id'        => 'desc',
            'name'      => esc_html__('Testimonial Description', 'lutron'),
            'desc'      => esc_html__('Enter the description for this Testimonial', 'lutron'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

         /* --------------------------------------------------------------
           5.2- HOME: AWARDS SECTION
        -------------------------------------------------------------- */
         $cmb_home_awards = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_awards_metabox',
            'title'         => esc_html__('Home: Awards Section', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $cmb_home_awards->add_field(array(
            'id'        => parent::PREFIX . 'home_awards_gallery',
            'name' => esc_html__('Logos Awards', 'lutron'),
            'desc' => esc_html__('Select logos for this Awards Section', 'lutron'),
            'type' => 'file_list',
            'preview_size' => array(50, 50),
            'query_args' => array('type' => 'image')
        ));

         /* --------------------------------------------------------------
            5.3- HOME: NEW BAR SECTION
        -------------------------------------------------------------- */
         $cmb_home_bar = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_bar_metabox',
            'title'         => esc_html__('Home: New Bar Section', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $cmb_home_bar->add_field(array(
            'id'        => parent::PREFIX . 'home_bar_text',
            'name'      => esc_html__('Section Description', 'lutron'),
            'desc'      => esc_html__('Enter the description for this Section', 'lutron'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

         $cmb_home_bar->add_field(array(
            'id'        => parent::PREFIX . 'home_bar_btn_text',
            'name'      => esc_html__('Button Text', 'lutron'),
            'desc'      => esc_html__('Enter the text for this button', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_bar->add_field(array(
            'id'        => parent::PREFIX . 'home_bar_btn_link',
            'name'      => esc_html__('Button Link', 'lutron'),
            'desc'      => esc_html__('Enter the Link for this button', 'lutron'),
            'type'      => 'text_url'
        ));
 
         /* --------------------------------------------------------------
           6.- HOME: PORTFOLIO SECTION
        -------------------------------------------------------------- */
         $cmb_home_portfolio = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_portfolio_metabox',
            'title'         => esc_html__('Home: Portfolio Section', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $cmb_home_portfolio->add_field(array(
            'id'        => parent::PREFIX . 'home_portfolio_subtitle',
            'name'      => esc_html__('Subtitle Section', 'lutron'),
            'desc'      => esc_html__('Enter the subtitle for this section', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_portfolio->add_field(array(
            'id'        => parent::PREFIX . 'home_portfolio_title',
            'name'      => esc_html__('Title Section', 'lutron'),
            'desc'      => esc_html__('Enter the title for this section', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_portfolio->add_field(array(
            'id'        => parent::PREFIX . 'home_portfolio_gallery',
            'name' => esc_html__('Photo Gallery', 'lutron'),
            'desc' => esc_html__('Select photos for this gallery', 'lutron'),
            'type' => 'file_list',
            'preview_size' => array(50, 50),
            'query_args' => array('type' => 'image')
        ));

         /* --------------------------------------------------------------
             7.- HOME: ABOUT SECTION
         -------------------------------------------------------------- */
         $cmb_home_about = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_about_metabox',
            'title'         => esc_html__('Home: About Section', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $cmb_home_about->add_field(array(
            'id'        => parent::PREFIX . 'home_about_subtitle',
            'name'      => esc_html__('Subtitle Section', 'lutron'),
            'desc'      => esc_html__('Enter the subtitle for this section', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_about->add_field(array(
            'id'        => parent::PREFIX . 'home_about_title',
            'name'      => esc_html__('Title Section', 'lutron'),
            'desc'      => esc_html__('Enter the title for this section', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_about->add_field(array(
            'id'        => parent::PREFIX . 'home_about_desc',
            'name'      => esc_html__('Description Section', 'lutron'),
            'desc'      => esc_html__('Enter the description for this Section', 'lutron'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

         /* --------------------------------------------------------------
             8.- HOME: CONTACT SECTION
         -------------------------------------------------------------- */
         $cmb_home_contact = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_contact_metabox',
            'title'         => esc_html__('Home: Contact Section', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $cmb_home_contact->add_field(array(
            'id'        => parent::PREFIX . 'home_contact_subtitle',
            'name'      => esc_html__('Subtitle Section', 'lutron'),
            'desc'      => esc_html__('Enter the subtitle for this section', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_contact->add_field(array(
            'id'        => parent::PREFIX . 'home_contact_title',
            'name'      => esc_html__('Title Section', 'lutron'),
            'desc'      => esc_html__('Enter the title for this section', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_contact->add_field(array(
            'id'        => parent::PREFIX . 'home_sendinblue_id',
            'name'      => esc_html__('Sendinblue List ID', 'lutron'),
            'desc'      => esc_html__('Enter the List ID for this contact form', 'lutron'),
            'type'      => 'text'
        ));

         $cmb_home_contact->add_field(array(
            'id'        => parent::PREFIX . 'home_contact_map',
            'name'      => esc_html__('Map Section', 'lutron'),
            'desc'      => esc_html__('Enter the title for this section', 'lutron'),
            'type'      => 'textarea_code'
        ));

        /* --------------------------------------------------------------
             9.- HOME: WHO WE ARE
         -------------------------------------------------------------- */
         $cmb_home_team = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_team_metabox',
            'title'         => esc_html__('Home: Equipo', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        
        $cmb_home_team->add_field(array(
            'id'        => parent::PREFIX . 'home_team_title',
            'name'      => esc_html__('Title Section', 'lutron'),
            'desc'      => esc_html__('Enter the title for this section', 'lutron'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

         $group_field_id = $cmb_home_team->add_field(array(
            'id'            => parent::PREFIX . 'home_team_group',
            'name'          => esc_html__('Grupo de Equipo', 'lutron'),
            'description'   => __('Items inside Section', 'lutron'),
            'type'          => 'group',
            'options'       => array(
                'group_title'       => __('Item {#}', 'lutron'),
                'add_button'        => __('Add Other Item', 'lutron'),
                'remove_button'     => __('Remove Item', 'lutron'),
                'sortable'          => true,
                'closed'            => true,
                'remove_confirm'    => esc_html__('Are you sure to remove this Item?', 'lutron')
            )
        ));
        
         $cmb_home_team->add_group_field($group_field_id, array(
            'id'        => 'image',
            'name'      => esc_html__('Image', 'lutron'),
            'desc'      => esc_html__('Upload an Image for this item', 'lutron'),
            'type'      => 'file',
            'options'   => array(
                'url'   => false
            ),
            'text'      => array(
                'add_upload_file_text' => esc_html__('Upload Image', 'lutron'),
            ),
            'query_args' => array(
                'type'  => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            ),
            'preview_size' => 'thumbnail'
        ));
        
         $cmb_home_team->add_group_field($group_field_id, array(
            'id'        => 'name',
            'name'      => esc_html__('Item Title', 'lutron'),
            'desc'      => esc_html__('Enter the title for this item', 'lutron'),
            'type'      => 'text'
        ));

           $cmb_home_team->add_group_field($group_field_id, array(
            'id'        => 'position',
            'name'      => esc_html__('Item Position', 'lutron'),
            'desc'      => esc_html__('Enter the Position for this item', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_home_team->add_group_field($group_field_id, array(
            'id'        => 'email',
            'name'      => esc_html__('Item Email', 'lutron'),
            'desc'      => esc_html__('Enter the email for this item', 'lutron'),
            'type'      => 'text'
        ));

        $cmb_home_team->add_group_field($group_field_id, array(
            'id'        => 'telf',
            'name'      => esc_html__('Item Phone', 'lutron'),
            'desc'      => esc_html__('Enter the phone for this item', 'lutron'),
            'type'      => 'text'
        ));

         /* --------------------------------------------------------------
           5.2- HOME: CAROUSEL SECTION
        -------------------------------------------------------------- */
        $cmb_home_carousel = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'home_carousel_metabox',
            'title'         => esc_html__('Home: Carousel Section', 'lutron'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => array('templates/page-home.php', 'templates/page-landingA.php', 'templates/page-landingB.php')),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

         $cmb_home_carousel->add_field(array(
            'id'        => parent::PREFIX . 'home_carousel_gallery',
            'name' => esc_html__('Images for Carousel', 'lutron'),
            'desc' => esc_html__('Select logos for this Carousel Section', 'lutron'),
            'type' => 'file_list',
            'preview_size' => array(50, 50),
            'query_args' => array('type' => 'image')
        ));


        
     }
 }

new customHomeMetaboxes;

let passd = true,
    windowHeight = 0,
    headerHeight = 0,
    currrentSlide = 0,
    allSlides = 0,
    formContact = document.getElementById('contactForm'),
    buttonContact = document.getElementById('contactSubmit'),
    formHero = document.getElementById('heroForm'),
    btnHero = document.getElementById('heroSubmit'),
    formSection = document.getElementById('sectionForm'),
    btnSection = document.getElementById('sectionSubmit'),
    headerCnt = document.getElementById('headerCnt'),
    cntGallery = document.getElementById('cntGallery'),
    wrapperGallery = document.getElementById('wrapperGallery'),
    closeGallery = document.getElementById('closeGallery'),
    prevGallery = document.getElementById('prevGallery'),
    nextGallery = document.getElementById('nextGallery'),
    galleryBtns = document.getElementsByClassName('gallery-item-wrap'),
    btnTop = document.getElementById('btnTop');

/* --------------------------------------------------------------
    MAIN FUNCTIONS
-------------------------------------------------------------- */
/* CUSTOM ON LOAD FUNCTIONS */
function lutronCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');

    window.onscroll = function(e) {
        headerSticky();
    }

    allSlides = document.querySelectorAll('.gallery-item-wrap').length;

    jQuery('input[name="contactPhone"]').inputmask({
        mask: "(999) 999-9999",
        greedy: false
    });

    jQuery('input[name="heroPhone"]').inputmask({
        mask: "(999) 999-9999",
        greedy: false
    });


    jQuery('input[name="sectionPhone"]').inputmask({
        mask: "(999) 999-9999",
        greedy: false
    });
    Array.from(galleryBtns).forEach((element) => {
        element.addEventListener('click', function(e) {
            var attribute = this.getAttribute('data-full');
            currrentSlide = parseInt(this.getAttribute('data-position'));
            cntGallery.classList.remove('d-none');
            renovateImageGallery(attribute);
        });
    });

    if (formContact) {
        buttonContact.addEventListener('click', function(e) {
            e.preventDefault();
            passd = true;

            var elementsForm = document.getElementsByClassName('landing-form-input');

            for (var i = 0; i < elementsForm.length; i++) {
                validateElement(elementsForm[i].id, elementsForm[i].getAttribute('type'), 'contact');
            }

            if (passd == true) {
                submitContactForm();
            }
        });
    }

    if (formHero) {
        btnHero.addEventListener('click', function(e) {
            e.preventDefault();
            passd = true;

            var elementsForm = document.getElementsByClassName('hero-form-input');

            for (var i = 0; i < elementsForm.length; i++) {
                validateElement(elementsForm[i].id, elementsForm[i].getAttribute('type'), 'hero');
            }

            if (passd == true) {
                submitHeroForm();
            }
        });
    }

    if (formSection) {
        btnSection.addEventListener('click', function(e) {
            e.preventDefault();
            passd = true;

            var elementsForm = document.getElementsByClassName('section-form-input');

            for (var i = 0; i < elementsForm.length; i++) {
                validateElement(elementsForm[i].id, elementsForm[i].getAttribute('type'), 'section');
            }

            if (passd == true) {
                submitSectionForm();
            }
        });
    }
}

document.addEventListener("DOMContentLoaded", lutronCustomLoad, false);

/* HEADER STICKER FUNCTION */
function headerSticky() {
    windowHeight = window.pageYOffset;

    if (windowHeight > 50) {
        headerCnt.classList.add('header-sticky');
    } else {
        headerCnt.classList.remove('header-sticky');
    }

    if (windowHeight > 250) {
        btnTop.classList.add('btn-top-visible');
    } else {
        btnTop.classList.remove('btn-top-visible');
    }
}

/* SWIPER FUNCTIONS */
const swiper = new Swiper('.testimonials-swiper', {
    slidesPerView: 1,
    loop: true,
    autoplay: {
        delay: 5000,
    }
});

/* GALLERY FUNCTIONS */
// CLOSE GALLERY
closeGallery.addEventListener('click', function() {
    wrapperGallery.innerHTML = '';
    cntGallery.classList.add('d-none');
});

// CHANGE GALLERY IMAGE
function renovateImageGallery(image) {
    wrapperGallery.innerHTML = '';
    wrapperGallery.innerHTML = '<img src="' + image + '" alt="gallery" class="img-fluid" />';
}

// PREV GALLERY IMAGE
prevGallery.addEventListener('click', function() {
    currrentSlide = currrentSlide + 1;
    if (currrentSlide < allSlides) {
        currrentSlide = allSlides;
    }
    var currentImage = document.getElementById('image-' + currrentSlide);
    var attribute = currentImage.getAttribute('data-full');
    currrentSlide = parseInt(currentImage.getAttribute('data-position'));
    renovateImageGallery(attribute);
});

// NEXT GALLERY IMAGE
nextGallery.addEventListener('click', function() {
    currrentSlide = currrentSlide + 1;
    if (currrentSlide > allSlides) {
        currrentSlide = 1;
    }
    var currentImage = document.getElementById('image-' + currrentSlide);
    var attribute = currentImage.getAttribute('data-full');
    currrentSlide = parseInt(currentImage.getAttribute('data-position'));
    renovateImageGallery(attribute);
});

/* --------------------------------------------------------------
    FORM FUNCTIONS
-------------------------------------------------------------- */

function isValidEmailAddress(emailAddress) {
    'use strict';
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function telephoneCheck(str) {
    var patt = new RegExp(/^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm);
    return patt.test(str);
}

function validateElement(id, type, origin) {

    str = id.replace(origin, '');
    switch (str) {
        case 'Name':
            var errorUrlString = custom_admin_url.error_name;
            var invalidUrlString = custom_admin_url.invalid_name;
            break;
        case 'Email':
            var errorUrlString = custom_admin_url.error_email;
            var invalidUrlString = custom_admin_url.invalid_email;
            break;
        case 'Phone':
            var errorUrlString = custom_admin_url.error_phone;
            var invalidUrlString = custom_admin_url.invalid_phone;
            break;
        case 'Find':
            var errorUrlString = custom_admin_url.error_find;
            var invalidUrlString = custom_admin_url.invalid_find;
            break;
        default:
            type = 'textarea'
            var errorUrlString = '';
            var invalidUrlString = '';
    }

    var element = document.getElementById(origin + str);
    var errorString = 'error' + origin + str;
    var elementError = document.getElementById(errorString);

    if (type != 'textarea') {
        if (element.value == '') {
            elementError.classList.remove('d-none');
            elementError.innerHTML = errorUrlString;
            passd = false;
        } else {
            switch (type) {
                case 'text':
                    if (element.value.length < 2) {
                        elementError.classList.remove('d-none');
                        document.getElementById(errorString).innerHTML = invalidUrlString;
                        passd = false;
                    } else {
                        elementError.classList.add('d-none');
                    }
                    break;
                case 'email':
                    if (isValidEmailAddress(element.value) == false) {
                        elementError.classList.remove('d-none');
                        elementError.innerHTML = invalidUrlString;
                        passd = false;
                    } else {
                        elementError.classList.add('d-none');
                    }
                    break;
                case 'tel':
                    if (telephoneCheck(element.value) == false) {
                        elementError.classList.remove('d-none');
                        elementError.innerHTML = invalidUrlString;
                        passd = false;
                    } else {
                        elementError.classList.add('d-none');
                    }
                    break;
                default:
                    elementError.classList.add('d-none');
            }
        }
    }
}

function submitContactForm() {
    var dataString = '';
    var elementsForm = document.getElementsByClassName('landing-form-input');

    dataString = 'action=custom_contact_send_message';
    for (var i = 0; i < elementsForm.length; i++) {
        var key = elementsForm[i].id;
        dataString += '&' + key + '=' + elementsForm[i].value;
    }

    dataString += '&listID=' + document.getElementById('listID').value;

    var elements = document.getElementsByClassName('contact-loader-css');
    elements[0].innerHTML = '<div class="loader"><div>';
    elements[0].classList.remove("d-none");
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        elements[0].classList.add("d-none");
        var result = JSON.parse(newRequest.responseText);
        if (result.success == true) {
            setTimeout(function() {
                console.log(custom_admin_url.thanks_url);
                url_redirect(custom_admin_url.thanks_url);
            }, 600);
        } else {
            alert(result.data);
        }
    };
    newRequest.send(dataString);

}

function submitHeroForm() {
    var dataString = '';
    var elementsForm = document.getElementsByClassName('hero-form-input');

    dataString = 'action=custom_hero_send_message';
    for (var i = 0; i < elementsForm.length; i++) {
        var key = elementsForm[i].id;
        dataString += '&' + key + '=' + elementsForm[i].value;
    }

    dataString += '&listID=' + document.getElementById('heroListID').value;

    var elements = document.getElementsByClassName('hero-loader-css');
    elements[0].innerHTML = '<div class="loader"><div>';
    elements[0].classList.remove("d-none");
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        var result = JSON.parse(newRequest.responseText);
        elements[0].classList.add("d-none");
        if (result.success == true) {
            setTimeout(function() {
                var url_thanks = formHero.getAttribute('data-thanksurl');
                url_redirect(url_thanks);
            }, 600);
        } else {
            alert(result.data);
        }
    };
    newRequest.send(dataString);

}

function submitSectionForm() {
    var dataString = '';
    var elementsForm = document.getElementsByClassName('section-form-input');

    dataString = 'action=custom_section_send_message';
    for (var i = 0; i < elementsForm.length; i++) {
        var key = elementsForm[i].id;
        dataString += '&' + key + '=' + elementsForm[i].value;
    }

    dataString += '&listID=' + document.getElementById('sectionListID').value;

    var elements = document.getElementsByClassName('section-loader-css');
    elements[0].innerHTML = '<div class="loader"><div>';
    elements[0].classList.remove("d-none");
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        var result = JSON.parse(newRequest.responseText);
        elements[0].classList.add("d-none");
        if (result.success == true) {
            setTimeout(function() {
                var url_thanks = formSection.getAttribute('data-thanksurl');
                url_redirect(url_thanks);
            }, 600);
        } else {
            alert(result.data);
        }
    };
    newRequest.send(dataString);

}

function url_redirect(url) {
    var X = setTimeout(function() {
        window.location.replace(url);
        return true;
    }, 300);

    if (window.location = url) {
        clearTimeout(X);
        return true;
    } else {
        if (window.location.href = url) {
            clearTimeout(X);
            return true;
        } else {
            clearTimeout(X);
            window.location.replace(url);
            return true;
        }
    }
    return false;
};

var swiperCarousel = new Swiper(".home-carousel", {
    slidesPerView: "auto",
    initialSlide: 1,
    centeredSlides: true,
    spaceBetween: 30,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 0,
        },
        767: {
          slidesPerView: 'auto',
        },
      },
    loop: true,
    autoplay: {
        delay: 5000,
    }
});
<div id="cntGallery" class="custom-modal-gallery d-none">
    <div id="wrapperGallery" class="custom-modal-gallery-wrapper">

    </div>
    <div id="closeGallery" class="gallery-close"><i class="fa fa-times" aria-hidden="true"></i></div>
    <div id="prevGallery" class="gallery-prev gallery-button"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
    <div id="nextGallery" class="gallery-next gallery-button"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
</div>

<a id="btnTop" href="#top" title="<?php _e('Back to top', 'lutron'); ?>" class="btn btn-md btn-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-start">
                    <?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
                    <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                        <ul id="sidebar-footer1" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-2' ) ) : ?>
                    <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                        <ul id="sidebar-footer2" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer-2' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-3' ) ) : ?>
                    <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                        <ul id="sidebar-footer3" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer-3' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-4' ) ) : ?>
                    <div class="footer-item col-xl col-lg col-md col-sm-12 col-12">
                        <ul id="sidebar-footer4" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer-4' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <div class="w-100"></div>
                    <div class="footer-copy col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h5><?php printf(__('Copyright &copy; 2021 Maxicon USA, All Rights Reserved | DEVELOPED BY <a href="%s">SMG</a> | <a href="%s">DIGITAL MARKETING AGENCY</a>', 'lutron'), 'http://www.screenmediagroup.com', 'http://www.screenmediagroup.com'); ?></h5>
                    </div>
                </div>
            </div>
        </div>

    </div>
</footer>
<?php wp_footer() ?>
</body>

</html>
<form id="heroForm" class="custom-hero-form col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-thanksurl="<?php echo esc_url(get_post_meta(get_the_ID(), 'ltr_landingA_thanks_page', true)); ?>">
    <div class="hero-input-item">
        <input id="heroName" name="heroName" type="text" class="form-control hero-form-input hero-name" placeholder="<?php _e('Your Name', 'lutron'); ?>" />
        <small id="errorheroName" class="danger d-none"></small>
    </div>
    <div class="hero-input-item">
        <input id="heroEmail" name="heroEmail" type="email" class="form-control hero-form-input hero-email" placeholder="<?php _e('Your Email', 'lutron'); ?>" />
        <small id="errorheroEmail" class="danger d-none"></small>
    </div>
    <div class="hero-input-item">
        <input id="heroPhone" name="heroPhone" type="text" class="form-control hero-form-input hero-phone" placeholder="<?php _e('Phone Number', 'lutron'); ?>" />
        <small id="errorheroPhone" class="danger d-none"></small>
    </div>
    <input type="hidden" name="listID" id="heroListID" value="<?php echo get_post_meta(get_the_ID(), 'ltr_landingA_sendinblue_list_id', true); ?>">
    <input type="hidden" name="title" id="title" class="form-control hero-form-input contact-message"  value="<?php echo get_the_title(); ?>" />
    <a id="heroSubmit" class="btn btn-md btn-hero"><?php echo get_post_meta(get_the_ID(), 'ltr_landingA_form_btn_text', true); ?></a>
    <div class="loader-css hero-loader-css d-none"></div>
    <div class="form-response col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"></div>
</form>
<section class="home-bar-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container">
        <div class="row align-items-center">
            <div class="home-bar-content-text col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12">
                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ltr_home_bar_text', true)); ?>
            </div>
            <div class="home-bar-content-button col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                <a class="btn btn-md btn-bar" href="<?php echo get_post_meta(get_the_ID(), 'ltr_home_bar_btn_link', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'ltr_home_bar_btn_text', true); ?>"><?php echo get_post_meta(get_the_ID(), 'ltr_home_bar_btn_text', true); ?></a>
            </div>
        </div>
    </div>
</section>
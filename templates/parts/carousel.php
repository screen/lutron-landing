<section class="main-home-carousel-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container-fluid">
        <div class="row">
            <div class="main-home-carousel-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php $arr_carousel = get_post_meta(get_the_ID(), 'ltr_home_carousel_gallery', true); ?>
                <?php if (!empty($arr_carousel))  { ?>
                <div class="swiper home-carousel">
                    <div class="swiper-wrapper">
                        <?php foreach ($arr_carousel as $key => $value) { ?>
                        <div class="swiper-slide">
                            <?php echo wp_get_attachment_image($key, 'large', false, array('class' => 'img-fluid')); ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
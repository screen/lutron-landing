<?php $arr_test = get_post_meta(get_the_ID(), 'ltr_home_testimonials', true);  ?>
<?php if (!empty($arr_test)) : ?>
<section class="home-testimonials-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container">
        <div class="row justify-content-center">
            <div class="home-testimonials-content col-xl-9 col-lg-10 col-md-11 col-sm-12 col-12">
                <div class="swiper-container testimonials-swiper">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <?php foreach ($arr_test as $item) { ?>
                        <div class="swiper-slide">
                            <div class="home-testimonials-item-wrapper">
                                <div class="star-container">
                                    <div class="i fa fa-star"></div>
                                    <div class="i fa fa-star"></div>
                                    <div class="i fa fa-star"></div>
                                    <div class="i fa fa-star"></div>
                                    <div class="i fa fa-star"></div>
                                </div>
                                <div class="desc-wrapper">
                                    <?php echo apply_filters('the_content', $item['desc']); ?>
                                </div>
                                <h3><?php echo $item['title']; ?></h3>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
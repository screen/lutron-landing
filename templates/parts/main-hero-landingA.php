<?php $bg_image_id = get_post_meta(get_the_ID(), 'ltr_home_hero_image_id', true); ?>
<?php $bg_image = wp_get_attachment_image_src($bg_image_id, 'full'); ?>
<section id="landinga" class="home-hero-container home-hero-landing-A-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_image[0]; ?>);">
    <?php $video_mp4 = get_post_meta(get_the_ID(), 'ltr_home_hero_video_mp4', true); ?>
    <?php $video_webm = get_post_meta(get_the_ID(), 'ltr_home_hero_video_webm', true); ?>
    <?php $video_ogv = get_post_meta(get_the_ID(), 'ltr_home_hero_video_ogv', true); ?>
    <video autoplay loop muted>
        <source src="<?php echo $video_mp4; ?>" type="video/mp4">
        <source src="<?php echo $video_webm; ?>" type="video/webm">
        <source src="<?php echo $video_ogv; ?>" type="video/ogg">
    </video>
    <div class="container-fluid">
        <div class="row align-items-xl-center align-items-lg-center align-items-md-center align-items-sm-start align-items-start row-hero">
            <div class="home-hero-content col-xl-5 offset-xl-1 col-lg-5 offset-lg-1 col-md-6 col-sm-12 col-12">
                <?php the_content(); ?>

                <div class="home-hero-buttons-wrapper">
                    <?php $button1_link = get_post_meta(get_the_ID(), 'ltr_home_hero_btn_link1', true); ?>
                    <?php if ($button1_link != '') { ?>
                    <?php $button1_text = get_post_meta(get_the_ID(), 'ltr_home_hero_btn_text1', true); ?>
                    <a href="<?php echo $button1_link; ?>" class="btn btn-md btn-hero" title="<?php echo $button1_text; ?>"><?php echo $button1_text; ?></a>
                    <?php } ?>

                    <?php $button2_link = get_post_meta(get_the_ID(), 'ltr_home_hero_btn_link2', true); ?>
                    <?php if ($button2_link != '') { ?>
                    <?php $button2_text = get_post_meta(get_the_ID(), 'ltr_home_hero_btn_text2', true); ?>
                    <a href="<?php echo $button2_link; ?>" class="btn btn-md btn-hero" title="<?php echo $button2_text; ?>"><?php echo $button2_text; ?></a>
                    <?php } ?>
                </div>
            </div>
            <div class="home-hero-form-content col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                <div class="home-hero-form-wrapper">
                    <div class="home-hero-form-text-container">
                        <?php $title = get_post_meta(get_the_ID(), 'ltr_landingA_form_title', true); ?>
                        <?php if ($title != '') { ?>
                        <h2><?php echo $title; ?></h2>
                        <?php } ?>
                        <?php $subtitle = get_post_meta(get_the_ID(), 'ltr_landingA_form_subtitle', true); ?>
                        <?php if ($subtitle != '') { ?>
                        <h3><?php echo $subtitle; ?></h3>
                        <?php } ?>
                    </div>
                    <?php echo get_template_part('templates/hero-contact-form'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
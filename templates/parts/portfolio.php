<section id="portfolio" class="home-portfolio-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container-fluid">
        <div class="row">
            <div class="subtitle-container text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h4><?php echo get_post_meta(get_the_ID(), 'ltr_home_portfolio_subtitle', true); ?></h4>
            </div>
            <div class="title-container text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h2><?php echo get_post_meta(get_the_ID(), 'ltr_home_portfolio_title', true); ?><strong>.</strong></h2>
            </div>
            <div class="portfolio-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php $arr_gallery = get_post_meta(get_the_ID(), 'ltr_home_portfolio_gallery', true); ?>
                <?php if (!empty($arr_gallery)) : ?>
                <?php $y = 1; ?>
                <?php foreach ($arr_gallery as $key => $value) { ?>
                <div id="gallery-<?php echo $y; ?>" class="gallery-item">
                    <div id="image-<?php echo $y; ?>" class="gallery-item-wrap" data-position="<?php echo $y; ?>" data-full="<?php echo $value; ?>">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                    <?php echo wp_get_attachment_image($key, 'box_item', false, array('class' => 'img-fluid')); ?>
                </div>
                <?php $y++; } ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php $i = 1; ?>
<?php $arr_group = get_post_meta(get_the_ID(), 'ltr_home_group1', true);  ?>
<?php $total_group = count($arr_group); ?>
<?php $half_group = floor($total_group / 2); ?>
<?php if (!empty($arr_group)) : ?>
<section class="home-items-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container-fluid p-0">
        <?php foreach ($arr_group as $item) { ?>
        <div class="row align-items-center no-gutters">
            <?php if ($i%2 == 0) { ?>
            <?php $class_image = 'order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1'; ?>
            <?php $class_text = 'order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12'; ?>
            <?php } else { ?>
            <?php $class_image = 'order-xl-1 order-lg-1 order-md-1 order-sm-1 order-1'; ?>
            <?php $class_text = 'order-xl-12 order-lg-12 order-md-12 order-sm-12 order-12'; ?>
            <?php } ?>
            <?php $bg_image = wp_get_attachment_image_src($item['image_id'], 'service_item'); ?>
            <div class="home-item-title col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 <?php echo $i . ' ' . $class_image; ?>" style="background: url(<?php echo $bg_image[0]; ?>);">
                <div class="home-item-title-wrapper">
                    <h2><?php echo $item['title']; ?><strong>.</strong></h2>
                </div>
            </div>
            <div class="home-item-desc col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 <?php echo $i . ' ' .  $class_text; ?>">
                <div class="home-item-desc-wrapper">
                    <?php echo $item['desc']; ?>
                </div>
            </div>
        </div>
        <?php if ($i == $half_group) { ?>
        <?php $activate_bar = get_post_meta(get_the_ID(), 'ltr_home_group_black_bar', true); ?>
        <?php if ($activate_bar == 'on') { ?>
        <section class="home-bar-container home-bar-inverted-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="home-bar-content-text col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ltr_home_bar_text', true)); ?>
                    </div>
                    <div class="home-bar-content-button col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                        <a class="btn btn-md btn-bar" href="<?php echo get_post_meta(get_the_ID(), 'ltr_home_bar_btn_link', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'ltr_home_bar_btn_text', true); ?>"><?php echo get_post_meta(get_the_ID(), 'ltr_home_bar_btn_text', true); ?></a>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
    </div>
</section>
<?php endif; ?>
<?php $arr_awards = get_post_meta(get_the_ID(), 'ltr_home_awards_gallery', true);  ?>
<?php if (!empty($arr_awards)) : ?>
<section class="home-awards-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container">
        <div class="row">
            <div class="home-awards-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h2><?php _e('Our Awards', 'lutron'); ?></h2>
            </div>
            <div class="home-awards-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php foreach ($arr_awards as $key => $value) { ?>
                <?php echo wp_get_attachment_image($key, 'logo', false, array('class' => 'img-fluid')); ?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<section id="landingb" class="main-landingb-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="landingb-main-form-container col-xl-5 offset-xl-1 col-lg-5 offset-lg-1 col-md-6 col-sm-12 col-12">
                <div class="home-hero-form-text-container">
                    <?php $subtitle = get_post_meta(get_the_ID(), 'ltr_landingB_form_subtitle', true); ?>
                    <?php if ($subtitle != '') { ?>
                    <h3><?php echo $subtitle; ?></h3>
                    <?php } ?>
                    <?php $title = get_post_meta(get_the_ID(), 'ltr_landingB_form_title', true); ?>
                    <?php if ($title != '') { ?>
                    <h2><?php echo $title; ?></h2>
                    <?php } ?>
                </div>
                <?php echo get_template_part('templates/section-contact-form'); ?>
            </div>
            <div class="landingb-main-image-container col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                <?php $bg_image_id = get_post_meta(get_the_ID(), 'ltr_landingB_form_image_id', true); ?>
                <?php echo wp_get_attachment_image($bg_image_id, 'full', false, array('class' => 'img-fluid')); ?>
            </div>
        </div>
    </div>
</section>
<section id="about" class="home-about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container">
        <div class="row justify-content-center">
            <div class="subtitle-container text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h4><?php echo get_post_meta(get_the_ID(), 'ltr_home_about_subtitle', true); ?></h4>
            </div>
            <div class="title-container text-center col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h2><?php echo get_post_meta(get_the_ID(), 'ltr_home_about_title', true); ?><strong>.</strong></h2>
            </div>
            <div class="w-100"></div>
            <div class="home-about-content col-xl-7 col-lg-7 col-md-9 col-sm-12 col-12">
                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ltr_home_about_desc', true)); ?>
            </div>
        </div>
    </div>
</section>
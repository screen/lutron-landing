<section class="main-home-team-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container">
        <div class="row">
            <div class="main-home-team-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ltr_home_team_title', true)); ?>
            </div>
            <?php $arr_team = get_post_meta(get_the_ID(), 'ltr_home_team_group', true); ?>
            <?php if (!empty($arr_team)) { ?>
            <div class="main-home-team-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row align-items-start justify-content-center">
                    <?php $total_team = count($arr_team); ?>
                    <?php if ($total_team > 4) { ?>
                    <?php $class = 'col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12'; ?>
                    <?php } else { ?>
                    <?php $class = 'col-xl col-lg col-md-4 col-sm-12 col-12'; ?>
                    <?php } ?>
                    <?php foreach ($arr_team as $item) { ?>
                    <article class="main-home-team-item <?php echo $class; ?>">
                        <div class="main-home-team-item-wrapper">
                            <div class="team-item-image">
                                <?php $bg_image_id = $item['image_id']; ?>
                                <?php echo wp_get_attachment_image($bg_image_id, 'boxed', false, array('class' => 'img-fluid')); ?>
                                <div class="team-metadata-container">
                                    <a href="mailto:<?php echo $item['email']; ?>"><?php echo $item['email']; ?></a>
                                    <a href="tel:<?php echo $item['telf']; ?>"><?php echo $item['telf']; ?></a>
                                </div>
                            </div>
                            <div class="team-content">
                                <h2><?php echo $item['name']; ?></h2>
                                <p><?php echo $item['position']; ?></p>
                            </div>
                        </div>
                    </article>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
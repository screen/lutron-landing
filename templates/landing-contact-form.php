<form id="contactForm" class="custom-contact-form col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="contact-input-item">
        <input id="contactName" name="contactName" type="text" class="form-control landing-form-input contact-name" placeholder="<?php _e('Your Name', 'lutron'); ?>" />
        <small id="errorcontactName" class="danger d-none"></small>
    </div>
    <div class="contact-input-item">
        <input id="contactEmail" name="contactEmail" type="email" class="form-control landing-form-input contact-email" placeholder="<?php _e('Your Email', 'lutron'); ?>" />
        <small id="errorcontactEmail" class="danger d-none"></small>
    </div>
    <div class="contact-input-item">
        <input id="contactPhone" name="contactPhone" type="text" class="form-control landing-form-input contact-phone" placeholder="<?php _e('Phone Number', 'lutron'); ?>" />
        <small id="errorcontactPhone" class="danger d-none"></small>
    </div>
    <div class="contact-input-item">
        <input id="contactFind" name="contactFind" type="text" class="form-control landing-form-input contact-find" placeholder="<?php _e('How did you find us?', 'lutron'); ?>" />
        <small id="errorcontactFind" class="danger d-none"></small>
    </div>
    <div class="contact-input-item">
        <textarea id="contactMessage" name="contactMessage" type="text" class="form-control landing-form-input contact-message" placeholder="<?php _e('Your Message', 'lutron'); ?>"></textarea>
    </div>
    <input type="hidden" name="listID" id="listID" value="<?php echo get_post_meta(get_the_ID(), 'ltr_home_sendinblue_id', true); ?>">
    <input type="hidden" name="title" id="title" class="form-control landing-form-input contact-message" value="<?php echo get_the_title(); ?>" />
    <a id="contactSubmit" class="btn btn-md btn-contact"><?php _e('Send Message', 'lutron'); ?></a>
    <div class="loader-css contact-loader-css d-none"></div>
    <div class="form-response col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"></div>
</form>
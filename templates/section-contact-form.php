<form id="sectionForm" class="custom-section-form col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-thanksurl="<?php echo esc_url(get_post_meta(get_the_ID(), 'ltr_landingB_thanks_page', true)); ?>">
    <div class="section-input-item">
        <input id="sectionName" name="sectionName" type="text" class="form-control section-form-input section-name" placeholder="<?php _e('Your Name', 'lutron'); ?>" />
        <small id="errorsectionName" class="danger d-none"></small>
    </div>
    <div class="section-input-item">
        <input id="sectionEmail" name="sectionEmail" type="email" class="form-control section-form-input section-email" placeholder="<?php _e('Your Email', 'lutron'); ?>" />
        <small id="errorsectionEmail" class="danger d-none"></small>
    </div>
    <div class="section-input-item">
        <input id="sectionPhone" name="sectionPhone" type="text" class="form-control section-form-input section-phone" placeholder="<?php _e('Phone Number', 'lutron'); ?>" />
        <small id="errorsectionPhone" class="danger d-none"></small>
    </div>
    <input type="hidden" name="listID" id="sectionListID" value="<?php echo get_post_meta(get_the_ID(), 'ltr_landingB_sendinblue_list_id', true); ?>">
    <input type="hidden" name="title" id="title" class="form-control section-form-input contact-message"  value="<?php echo get_the_title(); ?>" />
    <a id="sectionSubmit" class="btn btn-md btn-contact"><?php echo get_post_meta(get_the_ID(), 'ltr_landingB_form_btn_text', true); ?></a>
    <div class="loader-css section-loader-css d-none"></div>
    <div class="form-response col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"></div>
</form>
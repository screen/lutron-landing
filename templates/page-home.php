<?php
/**
* Template Name: Pagina Inicio
*
* @package lutron
* @subpackage lutron-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<?php $page_sections = get_post_meta(get_the_ID(), 'ltr_page_order', true); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php if (!empty($page_sections)) : ?>
        <?php foreach ($page_sections as $section) { ?>
        <?php switch ($section) {
                case 'main_hero':
                    get_template_part('templates/parts/main-hero');
                    break;

                case 'main_hero_landingA':
                    get_template_part('templates/parts/main-hero-landingA');
                    break;
                    
                case 'main_hero_landingB':
                    get_template_part('templates/parts/main-hero-landingB');
                    break;

                case 'services':
                    get_template_part('templates/parts/services');
                    break;

                case 'testimonials':
                    get_template_part('templates/parts/testimonials');
                    break;

                case 'awards':
                    get_template_part('templates/parts/awards');
                    break;

                case 'black_bar':
                    get_template_part('templates/parts/black_bar');
                    break;

                case 'portfolio':
                    get_template_part('templates/parts/portfolio');
                    break;

                case 'whoweare':
                    get_template_part('templates/parts/about');
                    break;

                case 'team':
                    get_template_part('templates/parts/team');
                    break;
                    
                case 'carousel':
                    get_template_part('templates/parts/carousel');
                    break;

                case 'contact':
                    get_template_part('templates/parts/contact');
                    break;
            }
            ?>
        <?php } ?>
        <?php endif; ?>
    </div>
</main>
<?php get_footer(); ?>
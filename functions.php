<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/* --------------------------------------------------------------
    REQUIRE VENDOR AUTOLOAD
-------------------------------------------------------------- */
require_once('vendor/autoload.php');


/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) {
    add_action('wp_enqueue_scripts', 'lutron_enqueue_jquery');
}
function lutron_enqueue_jquery()
{
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.6.0', false);
        wp_register_script('jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js', array('jquery'), '3.3.2', false);
    } else {
        wp_register_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js', false, '3.6.0', false);
        wp_register_script('jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.3.2.min.js', array('jquery'), '3.3.2', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */

require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

add_action('after_setup_theme', 'lutron_register_navwalker');
function lutron_register_navwalker()
{
    require_once('includes/class-wp-bootstrap-navwalker.php');
}

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

if (class_exists('WooCommerce')) {
    require_once('includes/wp_woocommerce_functions.php');
}

/* --------------------------------------------------------------
    ADD JETPACK COMPATIBILITY
-------------------------------------------------------------- */

if (defined('JETPACK__VERSION')) {
    require_once('includes/wp_jetpack_functions.php');
}

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus(array(
    'header_menu' => __('Menu Header', 'lutron'),
    'footer_menu' => __('Menu Footer', 'lutron'),
));

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action('widgets_init', 'lutron_widgets_init');

function lutron_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar Principal', 'lutron'),
        'id' => 'main_sidebar',
        'description' => __('Estos widgets seran vistos en las entradas y páginas del sitio', 'lutron'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));

    register_sidebars(4, array(
        'name'          => __('Pie de Página %d', 'lutron'),
        'id'            => 'sidebar_footer',
        'description'   => __('Estos widgets seran vistos en el pie de página del sitio', 'lutron'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ));

    if (class_exists('WooCommerce')) {
        register_sidebar(array(
            'name' => __('Sidebar de la Tienda', 'lutron'),
            'id' => 'shop_sidebar',
            'description' => __('Estos widgets seran vistos en Tienda y Categorias de Producto', 'lutron'),
            'before_widget' => '<li id="%1$s" class="widget %2$s">',
            'after_widget'  => '</li>',
            'before_title'  => '<h2 class="widgettitle">',
            'after_title'   => '</h2>',
        ));
    }
}

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD MAILCHIMP CONNECTOR
-------------------------------------------------------------- */

require_once('includes/class-sendinblue-connector.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(9999, 400, true);
}
if (function_exists('add_image_size')) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('logo', 250, 100, false);
    add_image_size('service_item', 1024, 500, array('center', 'center'));
    add_image_size('box_item', 250, 250, array('center', 'center'));
    add_image_size('boxed', 600, 600, array('center', 'center'));
}

/* --------------------------------------------------------------
    GET EMBED YOUTUBE
-------------------------------------------------------------- */
function get_custom_video_by_link($link)
{
    $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
    $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

    if (preg_match($longUrlRegex, $link, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }

    if (preg_match($shortUrlRegex, $link, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }

    $embed = 'https://www.youtube.com/embed/' . $youtube_id . '?rel=0&modestbranding=1';

    if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
        $id = $regs[3];
        $embed = "https://player.vimeo.com/video/" . $id;
    }

    return $embed;
}

/* --------------------------------------------------------------
    AJAX SEND MESSAGE
-------------------------------------------------------------- */
add_action('wp_ajax_custom_contact_send_message', 'custom_contact_send_message_handler');
add_action('wp_ajax_nopriv_custom_contact_send_message', 'custom_contact_send_message_handler');

function custom_contact_send_message_handler()
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }

    $fullname = $_POST['contactName'];
    $email = trim($_POST['contactEmail']);
    $phone = trim($_POST['contactPhone']);
    $find = trim($_POST['contactFind']);
    $message = $_POST['contactMessage'];
    $list_id = $_POST['listID'];
    $title = $_POST['title'];

    $name_split = explode(' ', $fullname);
    if (count($name_split) > 1) {
        $fname = $name_split[0];
        $lname = $name_split[1];
    } else {
        $fname = $name_split[0];
        $lname = '';
    }

    if (isset($phone)) {
        $phone = str_replace([ ' ', '(', ')', '-', '%20' ], '', $phone);
    }

    if (strpos($phone, '+1') !== false) {
        $phone = $phone;
    } else {
        $phone = '+1' . $phone;
    }

    $data = array(
        'FIRSTNAME'         => $fname,
        'LASTNAME'          => $lname,
        'SMS'               => $phone,
        'HOW_DO_YOU_FIND_US' => $find,
        'COMMENTS'          => $message,
        'CONTACT_SOURCE'    => 5,
        'SMG_LEAD'          => 1
    );

    $response = sendinblue_create_contact($email, $list_id, $data);

    ob_start();
    require_once get_theme_file_path('/templates/contact-email.php');
    $custom_logo_id = get_theme_mod('custom_logo');
    $image = wp_get_attachment_image_src($custom_logo_id, 'logo');
    $logo = $image[0];
    $body = ob_get_clean();
    $body = str_replace([
            '{fullname}',
            '{email}',
            '{phone}',
            '{find}',
            '{message}',
            '{logo}',
            '{title}'
        ], [
            $fullname,
            $email,
            $phone,
            $find,
            $message,
            $logo,
            $title,
        ], $body);

    $email_settings = get_option('lutron_email_settings');

    $to = $email_settings['main_email'];
            
    $emailsCC = $email_settings['cc_email'];
    $emailsBCC = $email_settings['bcc_email'];
            
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $headers[] = 'From: ' . esc_html(get_bloginfo('name')) . ' <noreply@' . strtolower($_SERVER['SERVER_NAME']) . '>';
    $headers[] = 'Cc: ' . $emailsCC;
    $headers[] = 'Bcc: ' . $emailsBCC;
            
    $subject = esc_html__('Lutron: New Lead', 'lutron');

    $sent = wp_mail($to, $subject, $body, $headers);

    if ($sent == false) {
        wp_send_json_error(esc_html__('Your message could not be sent. &nsbp; Please try again.', 'lutron'), 400);
    } else {
        wp_send_json_success(esc_html__("Thank you for submiting &nsbp; We will be contacting you in the next 24 hours.", 'lutron'), 200);
    }

    wp_die();
}

/* --------------------------------------------------------------
    AJAX HERO SEND MESSAGE
-------------------------------------------------------------- */
add_action('wp_ajax_custom_hero_send_message', 'custom_hero_send_message_handler');
add_action('wp_ajax_nopriv_custom_hero_send_message', 'custom_hero_send_message_handler');

function custom_hero_send_message_handler()
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }

    $fullname = $_POST['heroName'];
    $email = trim($_POST['heroEmail']);
    $phone = trim($_POST['heroPhone']);
    $list_id = $_POST['listID'];
    $title = $_POST['title'];

    $name_split = explode(' ', $fullname);
    if (count($name_split) > 1) {
        $fname = $name_split[0];
        $lname = $name_split[1];
    } else {
        $fname = $name_split[0];
        $lname = '';
    }

    if (isset($phone)) {
        $phone = str_replace([ ' ', '(', ')', '-', '%20' ], '', $phone);
    }

    if (strpos($phone, '+1') !== false) {
        $phone = $phone;
    } else {
        $phone = '+1' . $phone;
    }

    $data = array(
        'FIRSTNAME'         => $fname,
        'LASTNAME'          => $lname,
        'SMS'               => $phone,
        'CONTACT_SOURCE'    => 5,
        'SMG_LEAD'          => 1
    );

    $response = sendinblue_create_contact($email, $list_id, $data);

    ob_start();
    require_once get_theme_file_path('/templates/hero-contact-email.php');
    $custom_logo_id = get_theme_mod('custom_logo');
    $image = wp_get_attachment_image_src($custom_logo_id, 'logo');
    $logo = $image[0];
    $body = ob_get_clean();
    $body = str_replace([
            '{fullname}',
            '{email}',
            '{phone}',
            '{logo}',
            '{title}'
        ], [
            $fullname,
            $email,
            $phone,
            $logo,
            $title
        ], $body);

    $email_settings = get_option('lutron_email_settings');

    $to = $email_settings['main_email'];
            
    $emailsCC = $email_settings['cc_email'];
    $emailsBCC = $email_settings['bcc_email'];
            
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $headers[] = 'From: ' . esc_html(get_bloginfo('name')) . ' <noreply@' . strtolower($_SERVER['SERVER_NAME']) . '>';
    $headers[] = 'Cc: ' . $emailsCC;
    $headers[] = 'Bcc: ' . $emailsBCC;
            
    $subject = esc_html__('New Lead', 'lutron');

    $sent = wp_mail($to, $subject, $body, $headers);

    if ($sent == false) {
        wp_send_json_error(esc_html__('Your message could not be sent. &nsbp; Please try again.', 'lutron'), 400);
    } else {
        wp_send_json_success(esc_html__("Thank you for submiting &nsbp; We will be contacting you in the next 24 hours.", 'lutron'), 200);
    }

    wp_die();
}


/* --------------------------------------------------------------
    AJAX HERO SEND MESSAGE
-------------------------------------------------------------- */
add_action('wp_ajax_custom_section_send_message', 'custom_section_send_message_handler');
add_action('wp_ajax_nopriv_custom_section_send_message', 'custom_section_send_message_handler');

function custom_section_send_message_handler()
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }

    $fullname = $_POST['sectionName'];
    $email = trim($_POST['sectionEmail']);
    $phone = trim($_POST['sectionPhone']);
    $list_id = $_POST['listID'];
    $title = $_POST['title'];

    $name_split = explode(' ', $fullname);
    if (count($name_split) > 1) {
        $fname = $name_split[0];
        $lname = $name_split[1];
    } else {
        $fname = $name_split[0];
        $lname = '';
    }

    if (isset($phone)) {
        $phone = str_replace([ ' ', '(', ')', '-', '%20' ], '', $phone);
    }

    if (strpos($phone, '+1') !== false) {
        $phone = $phone;
    } else {
        $phone = '+1' . $phone;
    }

    $data = array(
        'FIRSTNAME'         => $fname,
        'LASTNAME'          => $lname,
        'SMS'               => $phone,
        'CONTACT_SOURCE'    => 5,
        'SMG_LEAD'          => 1
    );

    $response = sendinblue_create_contact($email, $list_id, $data);

    ob_start();
    require_once get_theme_file_path('/templates/hero-contact-email.php');
    $custom_logo_id = get_theme_mod('custom_logo');
    $image = wp_get_attachment_image_src($custom_logo_id, 'logo');
    $logo = $image[0];
    $body = ob_get_clean();
    $body = str_replace([
            '{fullname}',
            '{email}',
            '{phone}',
            '{logo}',
            '{title}'
        ], [
            $fullname,
            $email,
            $phone,
            $logo,
            $title
        ], $body);

    $email_settings = get_option('lutron_email_settings');

    $to = $email_settings['main_email'];
            
    $emailsCC = $email_settings['cc_email'];
    $emailsBCC = $email_settings['bcc_email'];
            
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $headers[] = 'From: ' . esc_html(get_bloginfo('name')) . ' <noreply@' . strtolower($_SERVER['SERVER_NAME']) . '>';
    $headers[] = 'Cc: ' . $emailsCC;
    $headers[] = 'Bcc: ' . $emailsBCC;
            
    $subject = esc_html__('New Lead', 'lutron');

    $sent = wp_mail($to, $subject, $body, $headers);

    if ($sent == false) {
        wp_send_json_error(esc_html__('Your message could not be sent. &nsbp; Please try again.', 'lutron'), 400);
    } else {
        wp_send_json_success(esc_html__("Thank you for submiting &nsbp; We will be contacting you in the next 24 hours.", 'lutron'), 200);
    }

    wp_die();
}
